/**
 * Created by Fara Aileen on 28/3/2016.
 */

var express = require("express"); // means: I want to use express framework
var app = express(); // I want to create an application


app.get("/hello", function(req,res){

//Status

    res.status(202); // status is accepted

    //MIME type; portions: general category, subcategory
    res.type("text/HTML");

    res.send("<p>HELLO to you too!</p>");
});

//3000 - port number
app.listen(3000, function() {
    console.info("Application started on port 3000");
});
